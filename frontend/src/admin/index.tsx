import { Admin, Resource } from "react-admin";
import jsonServerProvider from "ra-data-json-server";
import AccountList from "../components/AccountList";
import ProductList from "../components/ProductList";
// const dataProvider = jsonServerProvider("https://jsonplaceholder.typicode.com");

const dataProvider = jsonServerProvider(import.meta.env.VITE_BASE_URL);
const App = () => (
  <Admin dataProvider={dataProvider}>
    {/* <Resource name="posts" list={ListGuesser} />
    <Resource name="comments" list={ListGuesser} /> */}
    <Resource name="users" list={AccountList} />
    <Resource name="product" list={ProductList} />
  </Admin>
);

export default App;
